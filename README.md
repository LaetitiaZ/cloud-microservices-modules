# cloud-microservices-modules



## Structure

In order to avoid multiplication of repositories, all backend-microservices are reunited under this project as modules.
<br>
'account-service' microservice is in charge of the user accounts services. It is served on port 9090.
<br>
'book-service' microservice is in charge of managing books for the webservice. It is servec on port 8080.
<br>
'order-service' microservice is in charge of managing orders for the webservice. As the service would implicate real payment services, the microservice only serve an API for such logic, but without doing anything real (other than interacting with the order database). It is served on port 8081.
<br>
'gateway-service' microservice is the service called by the client. It is in charge of calling the different microservices previously listed. It is served on port 9094.

## Usage

With tools such as IntelliJ IDE, it is possible to allow concurrent run for a single project.
Here, if we want the microservices to run at the same time (as we are in a modules structures), it is necessary to edit run configuration to allow concurrent run. 
