package cloud.project.microservices.account.infrastructure;

import cloud.project.microservices.account.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserJPA extends JpaRepository<User, String> {

    Optional<User> findUserByAccountInfo_Mail(String mail);

}
