package cloud.project.microservices.account.application;

import cloud.project.microservices.account.domain.User;
import cloud.project.microservices.account.domain.UserRepository;
import cloud.project.microservices.account.security.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createAccount(User user) {
        String raw = user.getAccountInfo().getPassword();
        BCryptPasswordEncoder bCryptPasswordEncoder = Utils.encoder;
        String encodedPassword = bCryptPasswordEncoder.encode(raw);
        user.getAccountInfo().setPassword(encodedPassword);
        userRepository.add(user);
    }

    public User getUser(String mail) {
        return userRepository.findByMail(mail)
                .orElseThrow(() -> new IllegalArgumentException("No user found for mail " + mail));
    }

    public Optional<User> getOptionalUser(String mail) {
        return userRepository.findByMail(mail);
    }

}
