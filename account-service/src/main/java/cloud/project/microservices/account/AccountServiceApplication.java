package cloud.project.microservices.account;

import cloud.project.microservices.account.application.UserService;
import cloud.project.microservices.account.domain.AccountInfo;
import cloud.project.microservices.account.domain.User;
import cloud.project.microservices.account.security.CustomAuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class AccountServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountServiceApplication.class, args);
    }

    @Autowired
    private CustomAuthProvider authProvider;

    @Autowired
    private UserService userService;

    @PostMapping("/authenticate")
    public Authentication authenticate(@RequestBody AccountInfo accountInfo) {
        Authentication token = new UsernamePasswordAuthenticationToken(accountInfo.getMail(), accountInfo.getPassword());
        return authProvider.authenticate(token);
    }

    @PostMapping("/create-account")
    public void signup(@RequestBody User user) {
        userService.createAccount(user);
    }
}
