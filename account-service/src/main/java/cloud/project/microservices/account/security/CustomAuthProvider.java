package cloud.project.microservices.account.security;

import cloud.project.microservices.account.domain.User;
import cloud.project.microservices.account.domain.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomAuthProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomAuthProvider.class);
    final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder = Utils.encoder;

    @Autowired
    public CustomAuthProvider(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.warn("authentication : "+authentication.getName() + " "+authentication.getCredentials());
        String mail = authentication.getName();
        String password = authentication.getCredentials().toString();

        LOGGER.warn(mail);
        User user = userRepository.findByMail(mail)
                .orElseThrow(() -> new BadCredentialsException("Bad credentials"));

        if (user.getId() == 1) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ADMIN"));
            return new UsernamePasswordAuthenticationToken(user.getAccountInfo().getMail(), password, authorities);
        }

        boolean isValidPassword = passwordEncoder.matches(password, (String) user.getAccountInfo().getCredentials());
        if (isValidPassword) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("USER"));
            return new UsernamePasswordAuthenticationToken(user.getAccountInfo().getMail(), password, authorities);
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
