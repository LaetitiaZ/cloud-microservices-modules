package cloud.project.microservices.account.domain;

import javax.persistence.*;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String firstName;
    private String lastName;

    @Embedded
    private AccountInfo accountInfo;

    public User(String firstName, String lastName, AccountInfo accountInfo) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountInfo = accountInfo;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }
}
