package cloud.project.microservices.book.exposition.admin;

import cloud.project.microservices.book.domain.Book;
import cloud.project.microservices.book.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/books")
public class AdminBookController {

    @Autowired
    BookService bookService;

    @PostMapping("/add")
    public void addBook(@RequestBody Book book) {
        bookService.addBook(book);
    }

    @DeleteMapping("/{isbn}/delete")
    public void deleteBook(@PathVariable String isbn) {
        bookService.deleteBook(isbn);
    }
}
