package cloud.project.microservices.book.infrastructure;

import cloud.project.microservices.book.domain.Book;
import cloud.project.microservices.book.domain.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class BookRepositoryImpl implements BookRepository {

    private final BookJPA bookJPA;

    @Autowired
    public BookRepositoryImpl(BookJPA bookJPA) {
        this.bookJPA = bookJPA;
    }

    @Override
    public void add(Book book) {
        bookJPA.save(book);
    }

    public void update(Book book) {
        bookJPA.save(book);
    }

    @Override
    public void delete(Book book) {
        bookJPA.delete(book);
    }

    @Override
    public Optional<Book> findById(String id) {
        return bookJPA.findById(id);
    }

    @Override
    public void deleteById(String id) {
        bookJPA.deleteById(id);
    }

    @Override
    public List<Book> findAll() {
        return bookJPA.findAll();
    }

}
