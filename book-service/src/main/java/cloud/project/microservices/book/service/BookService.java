package cloud.project.microservices.book.service;

import cloud.project.microservices.book.domain.Book;
import cloud.project.microservices.book.domain.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookService.class);

    @Autowired
    BookRepository bookRepository;

    public void addBook(Book book) {
        bookRepository.add(book);
    }

    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    public Book getBook(String isbn) {
        return bookRepository.findById(isbn)
                .orElseThrow(() -> new IllegalArgumentException("No book found for id " + isbn));
    }

    public void deleteBook(String isbn) {
        bookRepository.deleteById(isbn);
    }

}
