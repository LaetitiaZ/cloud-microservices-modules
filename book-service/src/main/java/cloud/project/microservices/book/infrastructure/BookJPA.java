package cloud.project.microservices.book.infrastructure;

import cloud.project.microservices.book.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookJPA extends JpaRepository<Book, String> {
}
