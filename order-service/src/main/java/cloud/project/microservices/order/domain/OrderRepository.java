package cloud.project.microservices.order.domain;

public interface OrderRepository {

    void addOrder(Order order);
    void deleteOrderById(String id);
    Order getOrderById(String id);
}
