package cloud.project.microservices.order.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String isbn;
    private String userId;

    public Order(String isbn, String userId) {
        this.isbn = isbn;
        this.userId = userId;
    }

    public Order() {
    }

    public String getIsbn() {
        return isbn;
    }

    public String getUserId() {
        return userId;
    }
}
