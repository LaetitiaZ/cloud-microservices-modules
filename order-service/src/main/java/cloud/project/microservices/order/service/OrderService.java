package cloud.project.microservices.order.service;

import cloud.project.microservices.order.domain.Order;
import cloud.project.microservices.order.domain.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    public void addOrder(Order order) {
        orderRepository.addOrder(order);
    }

    //should add logic but implementation of a payment system is not the end goal here
    public void effectuateOrderPayment(Order order) {

    }

}
