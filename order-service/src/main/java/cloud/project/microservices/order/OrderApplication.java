package cloud.project.microservices.order;

import cloud.project.microservices.order.domain.Order;
import cloud.project.microservices.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    @Autowired
    OrderService service;

    @PostMapping("/order")
    public void order(@RequestBody Order order) {
        service.addOrder(order);

        //ask and effectuate payment
        service.effectuateOrderPayment(order);
    }
}
