package cloud.project.microservices.order.infrastructure;

import cloud.project.microservices.order.domain.Order;
import cloud.project.microservices.order.domain.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

    @Autowired
    OrderJPA orderJPA;

    @Override
    public void addOrder(Order order) {
        orderJPA.save(order);
    }

    @Override
    public void deleteOrderById(String id) {
        orderJPA.deleteById(id);
    }

    @Override
    public Order getOrderById(String id) {
        return orderJPA.getOne(id);
    }
}
