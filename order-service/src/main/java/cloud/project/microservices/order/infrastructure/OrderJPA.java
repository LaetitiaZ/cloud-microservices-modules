package cloud.project.microservices.order.infrastructure;

import cloud.project.microservices.order.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderJPA extends JpaRepository<Order, String> {
}
