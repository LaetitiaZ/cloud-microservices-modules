import cloud.project.microservices.web.domain.AccountInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonTest {
    @Test
    public void test1() throws IOException {

        AccountInfo info = new AccountInfo();
        info.setAuthenticated(true);
        info.getAuthorities().add(new SimpleGrantedAuthority("role1"));
        info.getAuthorities().add(new SimpleGrantedAuthority("role2"));

        String json1 = new ObjectMapper().writeValueAsString(info);
        AccountInfo readValue = new ObjectMapper().readValue(json1, AccountInfo.class);
        String json2 = new ObjectMapper().writeValueAsString(readValue);
        assertEquals(json1, json2);
    }
}
