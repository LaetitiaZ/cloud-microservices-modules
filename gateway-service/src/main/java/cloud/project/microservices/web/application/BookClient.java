package cloud.project.microservices.web.application;

import cloud.project.microservices.web.domain.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient("book-service-eureka-client")
public interface BookClient {

    @RequestMapping("/admin/books/add")
    public void addBook(@RequestBody Book book);

    @RequestMapping("/{isbn}/delete")
    public void deleteBook(@PathVariable String isbn);

    @RequestMapping("/books/display")
    public List<Book> getBooks();

    @RequestMapping("/books/{isbn}")
    public Book getBook(@PathVariable String isbn);
}
