package cloud.project.microservices.web.domain;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AccountInfoDeserializer extends JsonDeserializer<AccountInfo> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountInfoDeserializer.class);

    @Override
    public AccountInfo deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {


        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);

        List<String> authorities = new ArrayList<>();
        Iterator<JsonNode> elements = node.get("authorities").elements();
        while (elements.hasNext()) {
            JsonNode next = elements.next();
            JsonNode authority = next.get("authority");
            authorities.add(authority.asText());
        }


        String mail = node.get("principal").asText();
        String password = node.get("credentials").asText();

        AccountInfo info = new AccountInfo(mail, password, authorities);
        info.setAuthenticated(node.get("authenticated").booleanValue());

        return info;
    }
}
