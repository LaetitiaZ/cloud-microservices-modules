package cloud.project.microservices.web.application;

import cloud.project.microservices.web.domain.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("order-service-eureka-client")
public interface OrderClient {

    @RequestMapping("/order")
    public void order(@RequestBody Order order);
}
