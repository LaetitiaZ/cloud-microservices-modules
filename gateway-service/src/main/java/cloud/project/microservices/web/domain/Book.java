package cloud.project.microservices.web.domain;

import java.util.Date;

public class Book {

    private String isbn;
    private String title;
    private String author;
    private String genre;
    private String date;
    private float price;

    public Book(String isbn, String title, String author, String genre, String date, float price) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.date = date;
        this.price = price;
    }

    public Book() {
    }

    public String getGenre() {
        return genre;
    }

    public Book(String isbn) {
        this.isbn = isbn;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public float getPrice() {
        return price;
    }

    public String getDate() {
        return date;
    }


}
