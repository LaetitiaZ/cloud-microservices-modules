package cloud.project.microservices.web.domain;

public class Order {

    private String id;
    private String isbn;
    private String userId;

    public Order(String isbn, String userId) {
        this.isbn = isbn;
        this.userId = userId;
    }

    public Order() {
    }

    public String getIsbn() {
        return isbn;
    }

    public String getUserId() {
        return userId;
    }
}
