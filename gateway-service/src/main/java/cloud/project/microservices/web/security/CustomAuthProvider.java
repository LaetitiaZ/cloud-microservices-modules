package cloud.project.microservices.web.security;

import cloud.project.microservices.web.application.AccountClient;
import cloud.project.microservices.web.domain.AccountInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomAuthProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomAuthProvider.class);

    @Autowired
    AccountClient accountClient;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.warn(authentication.getName() + authentication.getCredentials().toString());
        String mail = authentication.getName();
        String password = authentication.getCredentials().toString();

        AccountInfo info = new AccountInfo(mail, password);
        LOGGER.info("entering login");
        return accountClient.login(info);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
