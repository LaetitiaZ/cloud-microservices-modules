package cloud.project.microservices.web.exposition;

import cloud.project.microservices.web.application.AccountClient;
import cloud.project.microservices.web.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Autowired
    AccountClient accountClient;

    @PostMapping("/signup")
    public void signup(@RequestBody User user) {
        accountClient.signup(user);
    }

    @GetMapping("/")
    public String home() {
        return "Welcome to the home page!";
    }
}
