package cloud.project.microservices.web.domain;

public class User {
    private int id;

    private String firstName;
    private String lastName;

    private AccountInfo accountInfo;

    public User(String firstName, String lastName, AccountInfo accountInfo) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountInfo = accountInfo;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public AccountInfo getAccountInfo() {
        return accountInfo;
    }
}
