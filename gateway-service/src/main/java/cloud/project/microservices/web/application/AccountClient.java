package cloud.project.microservices.web.application;

import cloud.project.microservices.web.domain.AccountInfo;
import cloud.project.microservices.web.domain.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("account-service-eureka-client")
public interface AccountClient {

    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    AccountInfo login(@RequestBody AccountInfo accountInfo);

    @RequestMapping("/create-account")
    void signup(@RequestBody User user);

}

