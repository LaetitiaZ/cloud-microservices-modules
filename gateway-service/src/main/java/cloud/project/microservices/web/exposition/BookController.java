package cloud.project.microservices.web.exposition;

import cloud.project.microservices.web.application.BookClient;
import cloud.project.microservices.web.application.OrderClient;
import cloud.project.microservices.web.domain.AccountInfo;
import cloud.project.microservices.web.domain.Book;
import cloud.project.microservices.web.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    BookClient bookClient;

    @Autowired
    OrderClient orderClient;

    @GetMapping("")
    public List<Book> getBooks() {
        return bookClient.getBooks();
    }

    @GetMapping("/{isbn}")
    public Book getBook(@PathVariable String isbn) {
        return bookClient.getBook(isbn);
    }

    @PostMapping("/admin/add")
    public void addBook(@RequestBody Book book) {
        bookClient.addBook(book);
    }

    @DeleteMapping("/admin/delete/{isbn}")
    public void deleteBook(@PathVariable String isbn) {
        bookClient.deleteBook(isbn);
    }

    @PostMapping("/order/{isbn}")
    public void orderBook(Authentication authentication, @PathVariable String isbn) {
        AccountInfo info = (AccountInfo) authentication.getPrincipal();
        Order order = new Order(isbn, info.getMail());
        orderClient.order(order);
    }
}
